
Welcome to Magento 2 installation! We're glad you chose to install Magento 2, a cutting edge, feature-rich eCommerce solution that gets results.

The installation instructions that used to be here are now published on our GitHub site. Use the information on this page to get started or go directly to the guide.

New to Magento? Need some help?

If you're not sure about the following, you probably need a little help before you start installing the Magento software:

Is the Magento software installed already?
What's a terminal, command prompt, or Secure Shell (ssh)?
Where's my Magento server and how do I access it?
What's PHP?
What's Apache?
What's MySQL?
Step 1: Verify your prerequisites

Use the following table to verify you have the correct prerequisites to install the Magento software.

Prerequisite	How to check	For more information
Apache 2.2 or 2.4	Ubuntu: apache2 -v
CentOS: httpd -v	Apache
PHP 5.5.x or 5.6.x	php -v	PHP Ubuntu
PHP CentOS
MySQL 5.6.x	mysql -u [root user name] -p	MySQL
Step 2: Prepare to install

After verifying your prerequisites, perform the following tasks in order to prepare to install the Magento software.

Install Composer
Clone the Magento repository
Step 3: Install and verify the installation

Update installation dependencies
Install Magento:
Install Magento software using the web interface
Install Magento software using the command line
Verify the installation
Contributing to the Magento 2 code base

Contributions can take the form of new components or features, changes to existing features, tests, documentation (such as developer guides, user guides, examples, or specifications), bug fixes, optimizations, or just good suggestions.

To learn about how to make a contribution, click here.

To learn about issues, click here. To open an issue, click here.

To suggest documentation improvements, click here.